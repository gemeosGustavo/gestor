package gestor.loja.gestor.domain.model;

import java.time.LocalDate;
import java.util.Date;

public class Estoque {

	private int idestoque;
	private int idfornecedor;
	private int idfuncionario;
	private int idjogo;
	private double valortotal;
	private LocalDate datarecebimento;
	private LocalDate datavencimento;
	
	
	
	public int getIdestoque() {
		return idestoque;
	}



	public void setIdestoque(int idestoque) {
		this.idestoque = idestoque;
	}



	public int getIdfornecedor() {
		return idfornecedor;
	}



	public void setIdfornecedor(int idfornecedor) {
		this.idfornecedor = idfornecedor;
	}



	public int getIdfuncionario() {
		return idfuncionario;
	}



	public void setIdfuncionario(int idfuncionario) {
		this.idfuncionario = idfuncionario;
	}



	public int getIdjogo() {
		return idjogo;
	}



	public void setIdjogo(int idjogo) {
		this.idjogo = idjogo;
	}



	public double getValortotal() {
		return valortotal;
	}



	public void setValortotal(double valortotal) {
		this.valortotal = valortotal;
	}



	public LocalDate getDatarecebimento() {
		return datarecebimento;
	}



	public void setDatarecebimento(LocalDate datarecebimento) {
		this.datarecebimento = datarecebimento;
	}



	public LocalDate getDatavencimento() {
		return datavencimento;
	}



	public void setDatavencimento(LocalDate datavencimento) {
		this.datavencimento = datavencimento;
	}



	@Override
	public String toString() {
		return "Estoque [idestoque=" + idestoque + ", idfornecedor=" + idfornecedor + ", idfuncionario=" + idfuncionario
				+ ", idjogo=" + idjogo + ", valortotal=" + valortotal + ", datarecebimento="
				+ datarecebimento + ", datavencimento=" + datavencimento + "]";
	}
	
	
	
	
}
