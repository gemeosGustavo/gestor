package gestor.loja.gestor.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "jogo")
public class Jogo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idjogo")
	private int idjogo;

	@Column(name = "nome")
	private String nome;

	@Column(name = "categoria")
	private String categoria;
	
	@Column(name = "descricao")
	private String descricao;

	@Column(name = "valor")
	private double valor;

	@Column(name = "tipo")
	private String tipo;

	@Column(name = "classificacao")
	private int classificacao;

	public int getIdjogo() {
		return idjogo;
	}

	public void setIdjogo(int idjogo) {
		this.idjogo = idjogo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(int classificacao) {
		this.classificacao = classificacao;
	}

	@Override
	public String toString() {
		return "Jogo [idjogo=" + idjogo + ", nome=" + nome + ", categoria=" + categoria + ", descricao=" + descricao
				+ ", valor=" + valor + ", tipo=" + tipo + ", classificacao=" + classificacao + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idjogo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jogo other = (Jogo) obj;
		if (idjogo != other.idjogo)
			return false;
		return true;
	}

}
