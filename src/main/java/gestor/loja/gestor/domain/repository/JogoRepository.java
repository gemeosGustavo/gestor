package gestor.loja.gestor.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gestor.loja.gestor.domain.model.Jogo;

@Repository
public interface JogoRepository extends JpaRepository<Jogo, Long> {

}
