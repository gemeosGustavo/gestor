package gestor.loja.gestor.domain.resource;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import gestor.loja.gestor.domain.model.Cliente;
import gestor.loja.gestor.domain.model.Estoque;
import gestor.loja.gestor.domain.model.Fornecedor;
import gestor.loja.gestor.domain.model.Funcionario;
import gestor.loja.gestor.domain.model.Jogo;

@RestController
@RequestMapping(value = "/cadastro")
public class GestorCadastro {

	@Autowired
	private RestTemplate restTemplate;
	

	// ----------------------------------------------------------------------------------------------------------------

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/cliente/add")
	public ResponseEntity<Cliente> addCliente(@Valid @RequestBody Cliente cliente) {

		String url = "http://127.0.0.1:7070/cliente/add";

		ResponseEntity<Cliente> vendasResponse = restTemplate.postForEntity(url, cliente, Cliente.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(cliente);

	}



	@CrossOrigin(origins = "*")
	@PostMapping(path = "/funcionario/add")
	public ResponseEntity<Funcionario> addFuncionario(@Valid @RequestBody Funcionario funcionario) {

		String url = "http://127.0.0.1:7070/funcionario/add";
	
		ResponseEntity<Funcionario> vendasResponse = restTemplate.postForEntity(url, funcionario, Funcionario.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(funcionario);

	}



	@CrossOrigin(origins = "*")
	@PostMapping(path = "/fornecedor/add")
	public ResponseEntity<Fornecedor> realizarVenda(@Valid @RequestBody Fornecedor fornecedor) {

		String url = "http://127.0.0.1:7070/fornecedor/add";
	

		ResponseEntity<Fornecedor> vendasResponse = restTemplate.postForEntity(url, fornecedor, Fornecedor.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(fornecedor);

	}



	@CrossOrigin(origins = "*")
	@PostMapping(path = "/jogo/add")
	public ResponseEntity<Jogo> realizarVenda(@Valid @RequestBody Jogo jogo) {

		String url = "http://127.0.0.1:7070/jogo/add";


		ResponseEntity<Jogo> vendasResponse = restTemplate.postForEntity(url, jogo, Jogo.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(jogo);

	}



	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Estoque> realizarVenda(@Valid @RequestBody Estoque estoque) {

		String url = "http://127.0.0.1:2222/estoque/adicionar";
		System.out.println(estoque);

		ResponseEntity<Estoque> vendasResponse = restTemplate.postForEntity(url, estoque, Estoque.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(estoque);

	}

// ----------------------------------------------------------------------------------------------------------------
	

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/cliente/getall")
	public ResponseEntity<List<Cliente>> buscarTodosClientes() {

		String url = "http://127.0.0.1:7070/cliente/getall";

		ResponseEntity<List<Cliente>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Cliente>>() {
				});

		List<Cliente> clienteLst = response.getBody();

		return new ResponseEntity<List<Cliente>>(clienteLst, HttpStatus.OK);

	}


	@CrossOrigin(origins = "*")
	@GetMapping(path = "/funcionario/getall")
	public ResponseEntity<List<Funcionario>> buscarTodosFuncionarios() {

		String url = "http://127.0.0.1:7070/funcionario/getall";

		ResponseEntity<List<Funcionario>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Funcionario>>() {
				});

		List<Funcionario> funcionarioLst = response.getBody();

		return new ResponseEntity<List<Funcionario>>(funcionarioLst, HttpStatus.OK);

	}
	

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/fornecedor/getall")
	public ResponseEntity<List<Fornecedor>> buscarTodosFornecedor() {

		String url = "http://127.0.0.1:7070/fornecedor/getall";

		ResponseEntity<List<Fornecedor>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Fornecedor>>() {
				});

		List<Fornecedor> fornecedorLst = response.getBody();

		return new ResponseEntity<List<Fornecedor>>(fornecedorLst, HttpStatus.OK);

	}
	

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/jogo/getall")
	public ResponseEntity<List<Jogo>> buscarTodosJogos() {

		String url = "http://127.0.0.1:7070/jogo/getall";

		ResponseEntity<List<Jogo>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Jogo>>() {
				});

		List<Jogo> jogoLst = response.getBody();

		return new ResponseEntity<List<Jogo>>(jogoLst, HttpStatus.OK);

	}
	
// -----------------------------------------------------------------------------------------------------------------------------
	
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/cliente/get/{id}")
	public ResponseEntity<Cliente> buscarCliente(@PathVariable("id") long id) {

		String url = "http://127.0.0.1:7070/cliente/get/" + id;

		ResponseEntity<Cliente> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Cliente>() {
				});

		Cliente cliente = response.getBody();

		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);

	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/funcionario/get/{id}")
	public ResponseEntity<Funcionario> buscarFuncionario(@PathVariable("id") long id) {

		String url = "http://127.0.0.1:7070/funcionario/get/" + id;

		ResponseEntity<Funcionario> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Funcionario>() {
				});

		Funcionario funcionario = response.getBody();

		return new ResponseEntity<Funcionario>(funcionario, HttpStatus.OK);

	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/fornecedor/get/{id}")
	public ResponseEntity<Fornecedor> buscarFornecedor(@PathVariable("id") long id) {

		String url = "http://127.0.0.1:2222/fornecedor/get/" + id;

		ResponseEntity<Fornecedor> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Fornecedor>() {
				});

		Fornecedor fornecedor = response.getBody();

		return new ResponseEntity<Fornecedor>(fornecedor, HttpStatus.OK);

	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/jogo/get/{id}")
	public ResponseEntity<Jogo> buscarJogo(@PathVariable("id") long id) {

		String url = "http://127.0.0.1:7070/jogo/get/" + id;

		ResponseEntity<Jogo> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Jogo>() {
				});

		Jogo jogo = response.getBody();

		return new ResponseEntity<Jogo>(jogo, HttpStatus.OK);

	}
	
//-----------------------------------------------------------------------------------------------------
	
	
}
