package gestor.loja.gestor.domain.resource;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import gestor.loja.gestor.domain.model.Estoque;

@RestController
@RequestMapping(value = "/estoque")
public class GestorEstoque {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Estoque> realizarVenda(@Valid @RequestBody Estoque estoque) {

		String url = "http://localhost:7765/estoque/adicionar";
		System.out.println(estoque);

		ResponseEntity<Estoque> vendasResponse = restTemplate.postForEntity(url, estoque, Estoque.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(estoque);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/all")
	public ResponseEntity<List<Estoque>> buscarVendas() {

		String url = "http://127.0.0.1:7765/estoque/dados";

		ResponseEntity<List<Estoque>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Estoque>>() {
				});

		List<Estoque> vendasLst = response.getBody();

		return new ResponseEntity<List<Estoque>>(vendasLst, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "get/{id}")
	public ResponseEntity<Estoque> buscarVenda(@PathVariable("id") long id) {

		String url = "http://127.0.0.1:7765/estoque/dados/" + id;

		ResponseEntity<Estoque> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Estoque>() {
				});

		Estoque estoque = response.getBody();

		return new ResponseEntity<Estoque>(estoque, HttpStatus.OK);

	}



}
