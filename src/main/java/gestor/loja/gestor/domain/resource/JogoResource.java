package gestor.loja.gestor.domain.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gestor.loja.gestor.domain.model.Jogo;
import gestor.loja.gestor.domain.repository.JogoRepository;

@RestController
@RequestMapping(value = "/jogo")
public class JogoResource {
	
	@Autowired
	private JogoRepository jogoRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Jogo> addJogo(@RequestBody Jogo jogo){
		
		Jogo newJogo = jogoRepository.save(jogo);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(newJogo);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Jogo>> getJogos(){	
		
		List<Jogo> lstJogo = jogoRepository.findAll();
		
		if(lstJogo.isEmpty()) {
			return new ResponseEntity<List<Jogo>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Jogo>>(lstJogo , HttpStatus.OK);
		
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}") 
	public ResponseEntity<Jogo> getJogo(@PathVariable("id") long id){
		
		Jogo capjogo = jogoRepository.findOne(id);
		
		if(capjogo == null) {
			return new ResponseEntity<Jogo>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Jogo>(capjogo, HttpStatus.OK);
	}
	

}
